package one.exercise;

// 1. Create Employee class with variables name, ssn, salary and behavior as printEmployeeInfo, printSocialSecurityandName
// Main method= Create 2 employee objects
// Use printEmployeeInfo and printSocialSecurityandName

public class Employee {

    String name;
    Integer ssn;
    Double salary;

    public Employee(String name, Integer ssn, Double salary) {
        this.name = name;
        this.ssn = ssn;
        this.salary = salary;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSsn() {
        return ssn;
    }

    public void setSsn(Integer ssn) {
        this.ssn = ssn;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public void printEmployeeInfo() {
        System.out.println("Employee Name: " + this.name + ", Salary: " + this.salary);
    }

    public void printScoialSecurityandName() {
        System.out.println("SSN: " + this.ssn + ", Name: " + this.name);
    }

    public static void main(String[] args) {

        Employee emp1 = new Employee("Roby", 789908767, 50000.00);
        Employee emp2 = new Employee("Rai", 119367474, 66666.66);

        emp1.printEmployeeInfo();
        emp1.printScoialSecurityandName();
        emp2.printEmployeeInfo();
        emp2.printScoialSecurityandName();
    }
}