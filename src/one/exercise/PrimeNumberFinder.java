package one.exercise;

public class PrimeNumberFinder {

    public static void main(String[] args) {

        System.out.print("Prime Numbers: ");

        for (int i = 1; i <= 100; i++) {

            int rem = 0;
            for (int j = 1; j <= i; j++) {
                if (i % j == 0) {
                    rem++;
                }
            }
            if (rem == 2) {
                System.out.print(i + ", ");
            }
        }
    }

}