package com.cerotid.bo;

import com.cerotid.bank.model.Account;
import com.cerotid.bank.model.Bank;
import com.cerotid.bank.model.Customer;
import com.cerotid.bank.model.Transaction;

import java.io.*;
import java.util.ArrayList;

public class BankBOImpl implements BankBO, Serializable {

    private Bank bank ;//= new Bank(); // Create constructor for bank and post construct...
    //
    // Bank is a singleton object
    BankBOImpl(){
        this.bank = Bank.getInstance();
    }

    @Override
    public void addCustomer(Customer customer) {
        bank.getCustomers().add(customer);

        //add logic should be changed to make sure customers are added to Database in addition to ArrayList
    }

    @Override
    public void openAccount(Customer customer, Account account) {
        customer.getAccounts().add(account);
    }

    @Override
    public void sendMoney(Customer customer, Transaction transaction) {
    }

    @Override
    public void depositMoneyInCustomerAccount(Customer customer) {
    }

    @Override
    public void editCustomerInfo(Customer customer) {
        bank.getCustomers();
    }

    @Override
    public Customer getCustomerInfo(int ssn) {
        deserializeBank();
        ArrayList<Customer> cusList = bank.getCustomers();
        //Customer cusReturn = new Customer();
        for (Customer cus : cusList) {
            if (ssn == (cus.getSsn())) {
                //cusReturn = cus;
                return cus;

            }
        }
        return null; //Create Custom Exception to throw CustomerDoNotExistException
    }

    @Override
    public void printBankStatus() {
        System.out.println(bank);
    }

    @Override
    public void serializeBank() {
        ArrayList<Customer> data = bank.getCustomers();
        try (ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream("data.ser"))) {
            os.writeObject(data);
        } catch (IOException i) {
            i.getMessage();
        }
    }

    @Override
    public void deserializeBank() {
        ArrayList<Customer> deserializedData;
        try (ObjectInputStream os = new ObjectInputStream(new FileInputStream("data.ser"))) {
            deserializedData = (ArrayList<Customer>) os.readObject();
            bank.setCustomers(deserializedData);
        } catch (IOException | ClassNotFoundException i) {
            i.getMessage();
        }
    }

    @Override
    public ArrayList<Customer> getCustomersByState(String stateCode) {
        deserializeBank();
        ArrayList<Customer> cusList = bank.getCustomers();
        ArrayList<Customer> filteredList = new ArrayList<>();
        for (Customer cus : cusList) {
            if (stateCode.equalsIgnoreCase(cus.getAddress().getStateCode())) {
                filteredList.add(cus);
            }
        }
        return filteredList;
    }


    @Override
    public void showCustomers() {
        deserializeBank();
        ArrayList<Customer> cusList = bank.getCustomers();
        for (Customer cus : cusList) {
            System.out.println(cus);
        }
    }
}