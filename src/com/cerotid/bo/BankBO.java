package com.cerotid.bo;

import java.util.ArrayList;
import java.util.List;

import com.cerotid.bank.model.Account;
import com.cerotid.bank.model.Customer;
import com.cerotid.bank.model.Transaction;

public interface BankBO {

    public void addCustomer(Customer customer);

    public void openAccount(Customer customer, Account account);

    public void sendMoney(Customer customer, Transaction transaction);

    public void depositMoneyInCustomerAccount(Customer customer);

    public void editCustomerInfo(Customer customer);

    public Customer getCustomerInfo(int ssn);

    public void printBankStatus();

    public void serializeBank();

    public void deserializeBank();

    public ArrayList<Customer> getCustomersByState(String stateCode);

    public void showCustomers();

}
