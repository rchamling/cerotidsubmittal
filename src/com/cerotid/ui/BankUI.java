package com.cerotid.ui;

import com.cerotid.bank.model.Account;
import com.cerotid.bank.model.AccountType;
import com.cerotid.bank.model.Address;
import com.cerotid.bank.model.Customer;
import com.cerotid.bo.BankBO;
import com.cerotid.bo.BankBOImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class BankUI {

    private BankBO bankBO = new BankBOImpl();

    private void displayOptions() {
        System.out.println("Choose Options (1-5)");
        System.out.println("1. Add Customer");
        System.out.println("2. Add Account");
        System.out.println("3. Send Money");
        System.out.println("4. Print Bank Status");
        System.out.println("5. Print Customers");
        System.out.println("6. Print Customers by State");
        System.out.println("7. Print Customers by SSN");
        System.out.println("8. Exit");
    }

    private int choose() { // Functions name should be descriptive
        int choice;
        System.out.print("Enter your selection (1-8): ");
        Scanner choose = new Scanner(System.in);
        choice = choose.nextInt();
        return choice;
    }

    private void chosen() {
        int res = choose();
        switch (res) {
            case 1:
                addCustomer();
                break;
            case 2:
                addAccount();
                break;

            case 3:
                sendMoney();
                break;
            case 4:
                printBankStatus();
                break;
            case 5:
                System.out.println("Printing Customers...");
                bankBO.showCustomers();
                break;
            case 6:
                System.out.print("Enter the state you want to search for customers: ");
                String filterByState = inputString();
                System.out.println("Printing Customers of " + filterByState);
                System.out.println(bankBO.getCustomersByState(filterByState));
                break;
            case 7:
                System.out.print("Enter the customer's SSN: ");
                int num = inputInt();
                try {
                    System.out.println(queryBySsn(num));
                } catch (Exception e) {
                    e.getMessage();
                }
                break;
            case 8:
                System.out.println("You choose: Exit");
                System.out.println("Exiting...");
                System.exit(0);
                break;
            default:
                System.out.println("Your Choice is Invalid");
                break;
        }
    }

    private void addCustomer() {
        System.out.println("Current customers data");

        bankBO.deserializeBank(); // Independent of the choices made
        bankBO.printBankStatus();

        System.out.println("You choose: Add Customer");
        String moreCus;

        //================================
        do {    // multiple do while loop...break down into methods and call them here
            System.out.print("Enter Customer First Name: ");
            String firstName = inputString();
            System.out.print("Enter Customer Last Name: ");
            String lastName = inputString();
            int ssn;
            do {
                System.out.print("Enter SSN: ");
                ssn = inputInt();
                while (bankBO.getCustomerInfo(ssn).getSsn() == ssn) {
                    System.out.print("The SSN is already taken. Try entering your SSN again: ");
                    ssn = inputInt();
                }
            } while (ssn > 999999999 || ssn < 100000000);
            System.out.print("Street Name: ");
            String streetName = inputString();
            System.out.print("City: ");
            String city = inputString();
            System.out.print("State: ");
            String stateCode = inputString();

            // Is there another way to do this?
            String zip;
            do {
                System.out.print("Zip: ");
                zip = inputString();
            } while (zip.length() != 5);

            Address add = new Address(streetName, zip, city, stateCode);

            Customer customer = new Customer(firstName, lastName, add, ssn);

            // ==============

            bankBO.addCustomer(customer);

            System.out.println("Current customers data");

            bankBO.printBankStatus();

            System.out.print("More Customer to add (Y/N)?");
            moreCus = inputString();    // whetherAddMoreCustomers
        } while (moreCus.equalsIgnoreCase("y"));

        bankBO.serializeBank();
    }

    private void addAccount() {

        // Existing Account or New Customer
        // Search by ssn OR add Customer
        // Retrieve Customer object by ssn
        // Create Account
        // Set customer
        // Serialize the customer

        System.out.println("You choose: Add Account");
        System.out.println("Enter \n1. Returning Customer\n" +
                "2. New Customer\n" +
                "3. Exit");
        int chosen = inputInt();
        switch (chosen) {
            case 1:
                System.out.print("Enter your SSN: ");
                int num = inputInt();
                Customer customer = queryBySsn(num);
                if (num == customer.getSsn()) { // TODO Not come here at all if there is no record
                    System.out.println("Your account details: ");
                    System.out.println(customer);
                } else {
                    System.out.println("You are not in the system.");
                    break;
                }
                accountCreation(num);
                bankBO.printBankStatus();
                break;
            case 2:
                addCustomer();
                addAccount();
                break;
            case 3:
                System.out.println("Exiting Account Creation Wizard.");
                break;
            default:
                System.out.println("Your choice is invalid.");
//                addAccount();
                break;
        }
    }

    private void sendMoney() {
        System.out.println("You choose: Send Money");
//                    new BankBOImpl().sendMoney();
    }

    private void printBankStatus() {
        System.out.println("You choose: Print Bank Status");
        bankBO.printBankStatus();
    }

    private String inputString() {
        Scanner sc = new Scanner(System.in);
        return sc.nextLine();
    }

    private int inputInt() {
        Scanner sc = new Scanner(System.in);
        return sc.nextInt();
    }

    private Double inputDbl() {
        Scanner sc = new Scanner(System.in);
        return sc.nextDouble();
    }

    private Customer queryBySsn(int ssn) {
        return bankBO.getCustomerInfo(ssn);
    }

    private void accountCreation(int ssn) {
        Customer customer = queryBySsn(ssn);
        ArrayList<Account> existingAccounts = customer.getAccounts();
        Account acc;
        String moreAcc;
        do {
            System.out.print("Enter Account Type (SAVING/CHECKING/BUSINESS): ");
            String typeOfAcc = inputString();
            while (!(typeOfAcc.equalsIgnoreCase("saving") || typeOfAcc.equalsIgnoreCase("checking") ||
                    typeOfAcc.equalsIgnoreCase("business"))) {
                System.out.print("Wrong choice of Account Type. Choose from SAVING/CHECKING/BUSINESS: ");
                typeOfAcc = inputString();
            }
            Date openDate = new Date();
            System.out.println("Opening date: " + openDate);
            System.out.print("Enter Starting Balance: ");
            Double accBalance = inputDbl();
            System.out.print("Close Account (Y/N)? ");
            String closeAcc = inputString();
            if (closeAcc.equalsIgnoreCase("y")) {
                Date closeDate = new Date();
                System.out.print("Closing date: " + closeDate);
                acc = new Account(AccountType.valueOf(typeOfAcc.toUpperCase()), openDate, closeDate, accBalance);
            } else {
                acc = new Account(AccountType.valueOf(typeOfAcc.toUpperCase()), openDate, accBalance);
            }
            if(existingAccounts != null){
                existingAccounts.add(acc);
                customer.setAccounts(existingAccounts );
            } else{
                ArrayList<Account> newAccount = new ArrayList<>();
                newAccount.add(acc);
                customer.setAccounts((newAccount));
            }

            bankBO.serializeBank();
            System.out.print("More Account to open (Y/N)?");
            moreAcc = inputString();
        } while (moreAcc.equalsIgnoreCase("y"));
    }

    public static void main(String[] args) {
        BankUI test = new BankUI();
        while (true) {
            test.displayOptions();
            test.chosen();
        }
    }
}
