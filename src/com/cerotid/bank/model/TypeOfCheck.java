package com.cerotid.bank.model;

public enum TypeOfCheck {
    PAPERCHECK, ECHECK;
}
