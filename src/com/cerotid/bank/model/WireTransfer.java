package com.cerotid.bank.model;

public class WireTransfer extends Transaction {
    private String beneficiaryFName;
    private String beneficiaryLName;
    private String intermediaryBankSwiftCode;
    private String beneficiaryBankName;
    private String beneficiaryAccNo;

    private WireTransfer(String beneficiaryFName, String beneficiaryLName, String intermediaryBankSwiftCode, String beneficiaryBankName, String beneficiaryAccNo) {
        this.beneficiaryFName = beneficiaryFName;
        this.beneficiaryLName = beneficiaryLName;
        this.intermediaryBankSwiftCode = intermediaryBankSwiftCode;
        this.beneficiaryBankName = beneficiaryBankName;
        this.beneficiaryAccNo = beneficiaryAccNo;
    }

    private WireTransfer(Double amount, Double fee, String receiverFirstName, String receiverLastName, String beneficiaryFName, String beneficiaryLName, String intermediaryBankSwiftCode, String beneficiaryBankName, String beneficiaryAccNo) {
        super(amount, fee, receiverFirstName, receiverLastName);
        this.beneficiaryFName = beneficiaryFName;
        this.beneficiaryLName = beneficiaryLName;
        this.intermediaryBankSwiftCode = intermediaryBankSwiftCode;
        this.beneficiaryBankName = beneficiaryBankName;
        this.beneficiaryAccNo = beneficiaryAccNo;
    }

    public String getBeneficiaryFName() {
        return beneficiaryFName;
    }

    public void setBeneficiaryFName(String beneficiaryFName) {
        this.beneficiaryFName = beneficiaryFName;
    }

    public String getBeneficiaryLName() {
        return beneficiaryLName;
    }

    public void setBeneficiaryLName(String beneficiaryLName) {
        this.beneficiaryLName = beneficiaryLName;
    }

    public String getIntermediaryBankSwiftCode() {
        return intermediaryBankSwiftCode;
    }

    public void setIntermediaryBankSwiftCode(String intermediaryBankSwiftCode) {
        this.intermediaryBankSwiftCode = intermediaryBankSwiftCode;
    }

    public String getBeneficiaryBankName() {
        return beneficiaryBankName;
    }

    public void setBeneficiaryBankName(String beneficiaryBankName) {
        this.beneficiaryBankName = beneficiaryBankName;
    }

    public String getBeneficiaryAccNo() {
        return beneficiaryAccNo;
    }

    public void setBeneficiaryAccNo(String beneficiaryAccNo) {
        this.beneficiaryAccNo = beneficiaryAccNo;
    }
}
