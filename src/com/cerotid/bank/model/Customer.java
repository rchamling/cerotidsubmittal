package com.cerotid.bank.model;

import java.io.*;
import java.util.ArrayList;

public class Customer implements Serializable{
    private String firstName;
    private String lastName;
    private ArrayList<Account> accounts;
    private Address address;
    private int ssn;

    public Customer(){
    }

    public Customer(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Customer(String firstName, String lastName, Address address, int ssn) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.ssn = ssn;
    }

    public Customer(String firstName, String lastName, ArrayList<Account> accounts, Address address, int ssn) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.accounts = accounts;
        this.address = address;
        this.ssn = ssn;
    }

    public void printCustomerDetails() {
        System.out.println("Printing Name and Address");
    }

    public void printCustomerAccounts() {
        System.out.println("Printing customer accounts...");

    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public ArrayList<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(ArrayList<Account> accounts) {
        this.accounts = accounts;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public int getSsn() {
        return ssn;
    }

    public void setSsn(int ssn) {
        this.ssn = ssn;
    }


    @Override
    public String toString() {
        return "Customer{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", accounts=" + accounts +
                ", address=" + address +
                ", ssn=" + ssn +
                '}';
    }
}
