package com.cerotid.bank.model;

import java.io.*;
import java.util.ArrayList;

public class Bank implements Serializable {
    private static Bank bankInstance = null;

    private String bankName;
    private ArrayList<Customer> customers; //Better Collection Class is Set based.
//    customers = deserialize();

    public static Bank getInstance(){
        if(bankInstance == null)
            bankInstance = new Bank();

        return bankInstance;
    }

    public Bank() {
        this.bankName =  "ABC Bank";
        this.customers = new ArrayList<>();
    }

    public Bank(ArrayList<Customer> customers){
        this.customers = customers;
    }

    public String getBankName() {
        return bankName;
    }


    public ArrayList<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(ArrayList<Customer> customers) {
        this.customers = customers;
    }

    public void printBankName() {
        System.out.println("Printing Bank Name..." + this.bankName);
    }

    public void printBankDetails() {
        System.out.println("Bank name:" + this.bankName + "\nCustomers: " + this.customers);
    }


    @Override
    public String toString() {
        return "Bank{" +
                "bankName='" + bankName + '\'' +
                ", customers=" + customers +
                '}';
    }
}
