package com.cerotid.bank.model;

import java.util.HashMap;

public class Transaction {
    private Double amount;
    private Double fee;
    private String receiverFirstName;
    private String receiverLastName;

    public Transaction(){

    }

    public Transaction(Double amount, Double fee, String receiverFirstName, String receiverLastName) {
        this.amount = amount;
        this.fee = fee;
        this.receiverFirstName = receiverFirstName;
        this.receiverLastName = receiverLastName;
    }

    public HashMap<String, String> createTransaction() {
        HashMap<String, String> data = new HashMap<String, String>();
        data.put("firstName", this.receiverFirstName);
        data.put("lastName", this.receiverLastName);
        data.put("amount", (this.amount).toString());

        return data;

    }

    public double deductAccountBalance() {
        double deduction = this.amount + this.fee;
        return deduction;

    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getFee() {
        return fee;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }

    public String getReceiverFirstName() {
        return receiverFirstName;
    }

    public void setReceiverFirstName(String receiverFirstName) {
        this.receiverFirstName = receiverFirstName;
    }

    public String getReceiverLastName() {
        return receiverLastName;
    }

    public void setReceiverLastName(String receiverLastName) {
        this.receiverLastName = receiverLastName;
    }
}
