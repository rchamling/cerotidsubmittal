package com.cerotid.bank.model;

public class ElectronicCheck extends Transaction {
    private TypeOfCheck typeOfCheck;

    public TypeOfCheck getTypeOfCheck() {
        return typeOfCheck;
    }

    public void setTypeOfCheck(TypeOfCheck typeOfCheck) {
        this.typeOfCheck = typeOfCheck;
    }

}
