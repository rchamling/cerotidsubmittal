package com.cerotid.bank.model;

public enum DeliveryOption {
    TENMINUTE, TWENTYFOURHRS;
}

