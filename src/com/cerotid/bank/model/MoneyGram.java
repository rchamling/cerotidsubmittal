package com.cerotid.bank.model;

public class MoneyGram extends Transaction {
    private DeliveryOption deliveryOption;
    private String destinationCountry;

    private MoneyGram(DeliveryOption deliveryOption, String destinationCountry) {
        this.deliveryOption = deliveryOption;
        this.destinationCountry = destinationCountry;
    }

    private MoneyGram(Double amount, Double fee, String receiverFirstName, String receiverLastName, DeliveryOption deliveryOption, String destinationCountry) {
        super(amount, fee, receiverFirstName, receiverLastName);
        this.deliveryOption = deliveryOption;
        this.destinationCountry = destinationCountry;
    }

    public DeliveryOption getDeliveryOption() {
        return deliveryOption;
    }

    public void setDeliveryOption(DeliveryOption deliveryOption) {
        this.deliveryOption = deliveryOption;
    }


    public String getDestinationCountry() {
        return destinationCountry;
    }

    public void setDestinationCountry(String destinationCountry) {
        this.destinationCountry = destinationCountry;
    }



}
