package com.cerotid.bank.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Scanner;

public class Account implements Serializable {
    private AccountType accountType;
    private Date accountOpenDate;
    private Date accountCloseDate;
    private Double amount;

    public Account() {
    }

    public Account(AccountType accountType, Date accountOpenDate, Double amount) {
        this.accountType = accountType;
        this.accountOpenDate = accountOpenDate;
        this.amount = amount;
    }

    public Account(AccountType accountType, Date accountOpenDate, Date accountCloseDate, Double amount) {
        this.accountType = accountType;
        this.accountOpenDate = accountOpenDate;
        this.accountCloseDate = accountCloseDate;
        this.amount = amount;
    }

    public void sendMoney() {
        System.out.print("Choose Account (Checking / Saving / Business");
        Scanner chooseAccount = new Scanner(System.in);
        String accountChosen = chooseAccount.nextLine();
        switch (accountChosen.toLowerCase()) {
            case "checking":
                System.out.println("Checking Account Chosen.");
                break;
            case "saving":
                System.out.println("Saving Account Chosen.");
                break;
            case "business":
                System.out.println("Business Account Chosen.");
                break;
        }

        System.out.print("Choose type of Transaction to Initiate (TenMinute / TwentyFourHour)");
        Scanner chooseTypeOfTransaction = new Scanner(System.in);
        String typeOfTransaction = chooseTypeOfTransaction.nextLine();

        switch (typeOfTransaction.toLowerCase()) {
            case "tenminute":
                System.out.println("Ten Minute Service Chosen.");
                break;
            case "twentyfourhour":
                System.out.println("Twenty Four Hour Service Chosen");
                break;
        }

        System.out.print("Complete the Transaction (Y / N): ");
        Scanner completeTransaction = new Scanner(System.in);
        String transactionCompleted = completeTransaction.nextLine();

        switch (transactionCompleted.toLowerCase()) {
            case "y":
                System.out.println("transaction completed");
                break;
            case "n":
                System.out.println("transaction discarded");
                break;
        }
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public Date getAccountOpenDate() {
        return accountOpenDate;
    }

    public void setAccountOpenDate(Date accountOpenDate) {
        this.accountOpenDate = accountOpenDate;
    }

    public Date getAccountCloseDate() {
        return accountCloseDate;
    }

    public void setAccountCloseDate(Date accountCloseDate) {
        this.accountCloseDate = accountCloseDate;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Account{" +
                "accountType=" + accountType +
                ", accountOpenDate='" + accountOpenDate + '\'' +
                ", accountCloseDate='" + accountCloseDate + '\'' +
                ", amount=" + amount +
                '}';
    }

    //    public void printAccountInfo() {
//        System.out.println("Choose Account");
//        Scanner chooseAccount = new Scanner(System.in);
//        String accountChosen = chooseAccount.nextLine();
//
//        System.out.println("Choose type of Transaction to Initiate (Checking / Saving / Business");
//        Scanner chooseTypeOfTransaction = new Scanner(System.in);
//        String typeOfTransaction = chooseTypeOfTransaction.nextLine();
//
//        System.out.println("Complete the Transaction");
//    }
}
