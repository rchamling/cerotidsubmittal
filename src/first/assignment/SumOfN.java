package first.assignment;

import java.util.Scanner;

// 4. Write a program that asks the user for a number n and prints the sum of the numbers 1 to n


public class SumOfN {

    public static void main(String[] args) {

        new SumOfN().run();

    }

    public void run() {

        System.out.print("Enter a number: ");
        Scanner num = new Scanner(System.in);
        int n = num.nextInt();
        this.sumNum(n);
    }

    public void sumNum(int n) {

        int res = 0;
        for (int i = 0; i <= n; i++) {
            res += i;
        }
        System.out.println("The sum of first " + n + " numbers is " + res);

    }


}