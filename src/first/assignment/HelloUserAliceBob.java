package first.assignment;

import java.util.Scanner;

public class HelloUserAliceBob {

    // 3. Modify the previous program such that only the users Alice and Bob are
    // greeted with their names.

    public static void main(String[] args) {

        new HelloUserAliceBob().greeting();

    }

    public String enterName() {

        System.out.println("Input user name: ");
        Scanner userInput = new Scanner(System.in);
        String enterName = userInput.nextLine();
        return enterName;

    }

    public void greeting() {

        while (true) {
            String name = enterName();
            if ("alice".equalsIgnoreCase(name) || "bob".equalsIgnoreCase(name)) {
                System.out.println("Hello " + name + "!");
                break;
            }
        }
    }
}
