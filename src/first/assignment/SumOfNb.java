package first.assignment;

import java.util.Scanner;

public class SumOfNb {

//	5. Modify the previous program such that only multiples of three or five are considered in the sum, e.g. 3, 5, 6, 9, 10, 12, 15 for n=17

    public void main(String[] args) {

        System.out.print("Enter a number: ");
        Scanner num = new Scanner(System.in);
        int n = num.nextInt();
        new SumOfNb().sumNum(n);

    }

    public void sumNum(int n) {

        int res = 0;
        for (int i = 0; i <= n; i++) {
            if (i % 3 == 0 || i % 5 == 0) {
                res += i;
            }
        }

        System.out.println("The sum of first " + n + " numbers is " + res);
    }

}
