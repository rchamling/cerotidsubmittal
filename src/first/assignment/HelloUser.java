package first.assignment;

import java.util.Scanner;

public class HelloUser {

    // 2. Write a program that asks the user for their name and greets them with
    // their name.

    public static void main(String[] args) {

        System.out.print("Input user name: ");
        Scanner userInput = new Scanner(System.in);
        String userName = userInput.nextLine();
        System.out.println("Hello " + userName + "!");

    }

}