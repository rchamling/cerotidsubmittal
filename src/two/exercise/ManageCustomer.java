package two.exercise;

import com.cerotid.bank.model.Customer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

class ManageCustomer {

    public List<Customer> loadFile(File fileName) {
        List<Customer> customerList = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = br.readLine()) != null) {
                ArrayList<String> nameList = new ArrayList<>(Arrays.asList(line.split(",")));
                customerList.add(new Customer(nameList.get(1), nameList.get(0)));
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found: " + fileName.toString());
        } catch (IOException e) {
            System.out.println("Unable to read file: " + fileName.toString());
        }
        return customerList;
    }

    public List<Customer> sortFile(List<Customer> customerList) {
        Collections.sort(customerList, new CustomSortCustomer());
        for (Customer cus : customerList) {
            System.out.println(cus.getLastName());
        }
        return customerList;
    }

    public void saveFile(List<Customer> customerList, File sortedFileName) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(sortedFileName))) {
            for (Customer customer : customerList) {
                bw.write(customer.getFirstName() + " " + customer.getLastName() + "\n");
            }
        } catch (IOException e) {
            System.out.println("Unable to read file: " + sortedFileName.toString());
        }
    }
}

class CustomSortCustomer implements Comparator<Customer> {
    @Override
    public int compare(Customer o1, Customer o2) {
        return o1.getLastName().compareToIgnoreCase(o2.getLastName());
    }
}
