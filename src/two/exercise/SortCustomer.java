/*
 * This sorts customer.txt files and output a file
 */

package two.exercise;

//1. Create a input file "Customers.txt" with 10 customer names -Format: :LastName, FirstName

//2. Write program to read input file and store customer in ArrayList and create a outputFile for customers in ascending order by their Last Name.

import com.cerotid.bank.model.Customer;

import java.io.File;
import java.util.List;

public class SortCustomer {

    public static void main(String[] args) {

        File fileName = new File("Customers.txt");
        File sortedFileName = new File("CustomerSorted.txt");

        ManageCustomer sortCus = new ManageCustomer();

        List<Customer> cusList = sortCus.loadFile(fileName);
        List<Customer> sortedList = sortCus.sortFile(cusList);

        System.out.println("Customer list is:\n===================");
        System.out.println(cusList);
        System.out.println("===================\nCustomer name sorted...\n===================");

        for ( Customer cus : sortedList){
            System.out.println(cus.getFirstName() + " " + cus.getLastName());
        }

        sortCus.saveFile(sortedList, sortedFileName);
    }
}
